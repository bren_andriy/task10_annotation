package com.bren.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class MyGenericClass<T> {
    private T object;

    private final Class<T> clazz;

    public MyGenericClass(Class<T> clazz) {
        this.clazz = clazz;
        try {
            object = clazz.getConstructor().newInstance();
            System.out.println("Declared fields of class: ");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field: fields) {
                System.out.println(" " + field.getName());
            }
            System.out.println();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


}
