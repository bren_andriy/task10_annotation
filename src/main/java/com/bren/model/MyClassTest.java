package com.bren.model;

import com.bren.annotations.MyAnnotation;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyClassTest {

    @MyAnnotation(value = 2)
    private String name;
    private String description;

    @MyAnnotation(value = 1, name = "Billy", description = "Some description")
    private int id;

    @MyAnnotation(value = 3, description = "Description", country = "Germany")
    private String country;

    @MyAnnotation(value = 4, country = "Poland")
    private void someMethod1() {
        System.out.println("Hello, I`m from some method. My name is : " + name);
    }

    @MyAnnotation(value = 5)
    private int someMethod2() {
        return id * 10;
    }

    @MyAnnotation(value = 6, name = "Artem")
    private String[] myMethod(String string, int... args) {
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = string + " -> " + (i + 1);
        }
        return strings;
    }

    private String[] myMethod(String... args) {
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = args[i] + " => " + i;
        }
        return strings;
    }

    @Override
    public String toString() {
        return "MyClassTest{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                ", country='" + country + '\'' +
                '}';
    }

}
