package com.bren.view;

import com.bren.annotations.MyAnnotation;
import com.bren.model.MyClassTest;
import com.bren.model.MyGenericClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner scanner = new Scanner(System.in);

    public MyView() {
        menu = new LinkedHashMap<>();
        new MyClassTest();
        menu.put("1", "  1 - Test fields");
        menu.put("2", "  2 - Test methods");
        menu.put("3", "  3 - Test Generic");
        menu.put("4", "  4 - Test object of unknown type");
        menu.put("5", "  5 - Invoke myMethod");
        menu.put("Q", "  Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        System.out.println("Test fields with annotations");
        Class clazz = MyClassTest.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myField = field.getAnnotation(MyAnnotation.class);
                int value = myField.value();
                String name = myField.name();
                int id = myField.id();
                String country = myField.country();
                String description = myField.description();
                System.out.println("Field: " + field.getName());
                System.out.println(" \tValue in @MyAnnotation: " + value);
                System.out.println(" \tName in @MyAnnotation: " + name);
                System.out.println(" \tId in @MyAnnotation: " + id);
                System.out.println(" \tCountry in @MyAnnotation: " + country);
                System.out.println(" \tDescription in @MyAnnotation: " + description);

            }
        }
    }

    private void pressButton2() {
        Class clazz = MyClassTest.class;
        Method[] methods = clazz.getDeclaredMethods();
        printMethods(methods);
    }

    private void pressButton3() {
        System.out.println("Test Generic");
        MyGenericClass<MyClassTest> myGenericClass = new MyGenericClass<>(MyClassTest.class);

    }

    private void pressButton4() {
        System.out.println("Test unknown object of unknown type");
        MyClassTest myClassTest = new MyClassTest();
        reflectionUnknownObject(myClassTest);
    }

    private void pressButton5() {
        System.out.println("Invoke myMethod(String a, int ... args) and myMethod(String … args)");
        MyClassTest myClassTest = new MyClassTest();
        Class clazz = myClassTest.getClass();
        int[] numbers = {1, 2, 3};
        try {
            Method method1 = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
            method1.setAccessible(true);
            String[] ints = (String[]) method1.invoke(myClassTest, "Hello", numbers);
            for (String i : ints) {
                System.out.println(i);
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        String[] strings = {"one", "two", "three"};
        try {
            Method method2 = clazz.getDeclaredMethod("myMethod", String[].class);
            method2.setAccessible(true);
            String[] strings2 = (String[]) method2.invoke(myClassTest, (Object) strings);
            for (String str: strings2) {
                System.out.println(str);
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void printMethods(Method[] methods) {
        for (Method method : methods) {
            System.out.println(" " + method.getName());
        }
    }

    private void printFields(Field[] fields) {
        for (Field field : fields) {
            System.out.println(" " + field.getName());
        }
    }

    private void reflectionUnknownObject(Object object) {
        try {
            Class clazz = object.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            System.out.println("The name of class is: " + clazz.getName());

            Constructor constructor = clazz.getConstructor();
            System.out.println("The name of constructor is: " + constructor.getName());
            System.out.println("The methods of class: ");

            Method[] methods = clazz.getMethods();
            printMethods(methods);
            System.out.println("The declared fields of class: ");
            Field[] fields = clazz.getDeclaredFields();
            printFields(fields);
            System.out.println();
            fields[0].setAccessible(true);
            if (fields[0].getType() == String.class) {
                fields[0].set(object, "Someone");
            }
            System.out.println(object);

            Method method = clazz.getDeclaredMethod("someMethod1");
            method.setAccessible(true);
            method.invoke(object);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
