package com.bren.view;

public interface Printable {
    void print();
}
